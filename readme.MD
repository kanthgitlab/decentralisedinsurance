# Decentralized Insurance Settlement:

## Solution:
- Create a decentralized solution where Patients, Hospitals and Insurance Providers can get on-boarded
- Patients will receive a unique DID and Insurance-Policy-Claim(s) from insurance Provider
- Patients will submit Verifiable Credentials to Hospital
- Hospital will encrypt the invoice with verifyKey (from customer) and store in IPFS
- Patient will send the IPFS Hash, Patient basic information along with Verifiable Credentials to Insurance Provider
- Insurance Provider will verify the credentials, patient invoice and initiate Payments to Patient

![Decentralized_Insurance](./Decentralized_Insurance.png)


# Create DID for Insurance Claim

## Background:

### Scenario-1:
1. Patients who need medical insurance can avail new insurance policy from insurance provider
2. Insurance Provider will collect Patient details and generate a sovrin-DID with insurance claim
3. DID is generated on sovrin node (Decentralised platform to create and verify credentials)

### Scenario-2:
1. Patient visits hospital and submit his/her DID (Issued by Insurance Provider)
2. Hospital verifies the DID and credentials of the Patient
3. Hospital issues Invoice to Patient 

### Scenario-3:
1. Patient submits the invoice to Insurance Provider
2. Patient submits his/her invoice along with DID and digital copy of invoice
3. Insurance Provider will verify credentials of Patient
4. Insurance Provider will verify authenticity of the Invoice
5. Insurance Provider makes payment after completion of 2, 3 or 4
6. Insurance Provider rejects the payment in case of failure in any of the steps: 2, 3 or 4 


## Sample Scenario:

<p>
Tracy has a sore throat soon after moving to a new town. 
She finds a physician through her health care network and goes in for treatment.
She is a new patient, so the clinic needs to know who she is and how she will be paying. 
 When checking in, she presents her verifiable credential that demonstrates her identity and her proof of insurance.
When the clinic submits this to the insurance company, they can automatically ascertain that she submitted her proof of identity and insurance to the provider and granted the physician the ability to submit the claim for payment. 
</p>

