function DigitalEntityModel(did, name, personalId,contactNo,verifyKey,encryptionPublicKey,
                            seed,signKey, documentHash, documentPath) {
        // Set properties
        this.did = did;
        this.name = name;
        this.personalId = personalId;
        this.contactNo = contactNo;
        this.verifyKey = verifyKey;
        this.encryptionPublicKey = encryptionPublicKey;
        this.seed = seed;
        this.signKey = signKey;
        this.documentHash = documentHash;
        this.documentPath = documentPath;
    }


module.exports = DigitalEntityModel