pragma solidity 0.5.16;

interface DIDIssuerInterface {

    function registerDID(address _did, string calldata _name, string calldata _personalId,  string calldata  _contactNo,
                        string calldata _category, string calldata _documentHash, string calldata _verifierKey) external returns(bool);

    function getDIDDetails(address _didAddress) external view
    //canAccessDIDDetails(_didAddress, address(msg.sender))
                                         returns(address didAddress , string memory name,
                                            string memory personalId, string memory contactNo,
                                             string memory category,
                                            string memory documentHash, string memory verifierKey);

    function getVerifierDetails(address _verifier) external view returns(address did,string memory licensedName,
                        string memory licenseNumber,
                        string memory documentHash,
                        string memory verifierKey,
                        uint issuedTime,uint revokedTime,bool isRevoked,
                        bool isActive);

     function doesDIDExist(address _did) external returns(bool isIndeed);

}