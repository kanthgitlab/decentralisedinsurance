/**
 *  Write the logic and database crud operation for DigitalEntity goes here in Service Layer.
 */
const DigitalEntity = require('../entity/DigitalEntity');
const DigitalEntityModel = require('../models/DigitalEntityModel');
const responseDataUtil = require('../util/ResponseDataUtil');
const ipfsService = require('../services/IPFSService');
const sovrinDID = require("sovrin-did");
const logger = require("../common/logger");

const DigitalEntityService = {

    /**
     * Create  a new DigitalEntity
     * @param digitalEntityRequestData
     * @returns {Promise<*>}
     */
    createDigitalEntity: async (digitalEntityRequestData) => {
        let responseData = null;
        logger.log(digitalEntityRequestData);
        let digitalEntityForPersistance = DigitalEntityService.convertToEntity(digitalEntityRequestData);
        logger.log(`digitalEntityForPersistance: ${digitalEntityForPersistance}`);
        let existingDigitalEntity =
        await DigitalEntity.findOne({personalId: digitalEntityForPersistance.personalId});

        // Check digitalEntity exist or not
        if (existingDigitalEntity) {
            responseData = responseDataUtil
                .create(400, false, 'DigitalEntity already exist and check personal id.',
                    existingDigitalEntity);
            return responseData;
        }

        let did = sovrinDID.gen();
        digitalEntityForPersistance.encryptionPublicKey = did.encryptionPublicKey;
        digitalEntityForPersistance.verifyKey = did.verifyKey;
        digitalEntityForPersistance.did = did.did;
        let ipfsModel = await ipfsService.addFileToIPFS({content: "Test Data"});
        logger.log(`documentHash is: ${ipfsModel.hash}`);
        digitalEntityForPersistance.documentHash = ipfsModel.ipfsHash;
        digitalEntityForPersistance.documentPath = ipfsModel.path;

        //save when digitalEntity does not exist.
        await digitalEntityForPersistance.save()
            .then(function (savedDigitalEntityObject) {
                responseData = responseDataUtil.create(201, true, "DigitalEntity Created.", DigitalEntityService.convertToModel(savedDigitalEntityObject, did));
            })
            .catch((err) => {
                console.log(err);
                responseData = responseDataUtil.create(500, false, 'An Error Occured');
            });


        return responseData;
    },

    /**
     * Create  Get all digitalEntitys in system
     * @returns {Promise<*>}
     */
    findAllDigitalEntities: async () => {
        let responseData = null;
        await DigitalEntity.find({})
            .then(function (digitalEntitys) {
                let digitalEntityModels = digitalEntitys.map(DigitalEntityService.convertEntityToModel);
                responseData = responseDataUtil.create(200, true, "Found digitalEntityModels.", digitalEntityModels);
            })
            .catch((err) => {
                console.log("Error in findAllEvents", err);
                responseData = responseDataUtil.createDefaultError();
            });
        return responseData;
    },

    /**
     * Find one by Id
     * @param id
     * @returns {Promise<*>}
     */
    findOneById: async (id) => {
        let responseData = null;
        await DigitalEntity.findById(id)
            .then(function (digitalEntity) {
                responseData = responseDataUtil.create(200, true, "Found DigitalPersons.", digitalEntity);
            })
            .catch((err) => {
                console.log("Error in findAllEvents", err);
                responseData = responseDataUtil.createDefaultError();
            });
        return responseData;
    },

    /**
     * Find one by parameters
     * @param id
     * @returns {Promise<*>}
     */
    findByParams: async (personalId) => {
        let digitalEntitys = await DigitalEntity.find({personalId: personalId});
        return responseDataUtil.create(200, true, "Found DigitalEntity by personalId.", digitalEntitys);
    },

    /**
     * Find one by did
     * @param id
     * @returns {Promise<*>}
     */
    findByParams: async (_did) => {
        let digitalEntitys = await DigitalEntity.find({did: _did});
        return responseDataUtil.create(200, true, "Found DigitalEntity by DID.", digitalEntitys);
    },


    /* DigitalEntity data validation and convert into Model */
     convertToModel(digitalEntityData) {
        return new DigitalEntityModel({
            did: digitalEntityData.did,
            name: digitalEntityData.name,
            personalId: digitalEntityData.personalId,
            contactNo: digitalEntityData.contactNo,
            verifyKey: digitalEntityData.verifyKey,
            encryptionPublicKey: digitalEntityData.encryptionPublicKey,
            documentHash : digitalEntityData.documentHash,
            documentPath : digitalEntityData.documentPath
        });
    },

     /* DigitalEntity data validation and convert into Model */
     convertEntityToModel(digitalEntityData) {
        return new DigitalEntityModel({
            did: digitalEntityData.did,
            name: digitalEntityData.name,
            personalId: digitalEntityData.personalId,
            contactNo: digitalEntityData.contactNo,
            verifyKey: digitalEntityData.verifyKey,
            encryptionPublicKey: digitalEntityData.encryptionPublicKey,
            documentHash : digitalEntityData.documentHash,
            documentPath : digitalEntityData.documentPath
        });
    },

    /* DigitalEntity data validation and convert into Entity */
    convertToEntity(digitalEntityRequestData) {
        let digitalEntity = new DigitalEntity();
        digitalEntity.name = digitalEntityRequestData.name;
        digitalEntity.personalId = digitalEntityRequestData.personalId;
        digitalEntity.contactNo = digitalEntityRequestData.contactNo;
        logger.info('digitalEntity: '+JSON.stringify(digitalEntity));
        return digitalEntity;
    }   ,

    /* only for testing */
    test: () => {
        console.log('only test DigitalEntity service method');
    }


};


module.exports = DigitalEntityService;
