function IPFSFileModel(ipfsHash, fileName, path) 
{
    this.ipfsHash = ipfsHash;
    this.fileName = fileName;
    this.path = path;
}

module.exports = IPFSFileModel