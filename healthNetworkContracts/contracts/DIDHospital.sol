pragma solidity 0.5.16;
import "./DIDIssuerInterface.sol";

//Contract to register/maintain Decentralized-Identities by Hospital-Network
contract DIDHospital {
    address payable private owner;
    address private hospitalNetworkDID;
    address private didIssuerAddress;
    DIDIssuerInterface private didIssuerInterface;

    //constructor
    constructor() public {
        owner = msg.sender;
    }

     // this function should be called should we ever want to change the didIssuerAddress
    function setDidIssuerAddress(address _didIssuerAddress) ownerOnly public returns (address){
        didIssuerAddress = _didIssuerAddress;
        didIssuerInterface = DIDIssuerInterface(didIssuerAddress);
        return didIssuerAddress;
    }

    struct Hospital {
        address did;
        string name;
        string contactNo;
        string hospitalAddress;
        string zipcode;
        string documentHash;
        string verifierKey;
        uint issuedTime;
        uint revokedTime;
        bool isRevoked;
        bool isActive;
        bool isEntity;
    }

    //hospitalDID => Hospital Struct
    mapping(address => Hospital) hospitals;

    //bytes32(hospitalName) => hospitalDID
    mapping(bytes32 => address) private hospitalNameAndHospitalDIDMap;

    //Struct for DIDPatientBlock
    struct Patient {
        address did;
        string name;
        string contactNo;
        string documentHash;
        string verifierKey;
        uint issuedTime;
        uint revokedTime;
        bool isRevoked;
        bool isActive;
        bool isEntity;
    }

    //patientDID => Patient Object
    mapping(address => Patient) private didPatientBlocks;

    //bytes32(patientLegalName) => PatientDID
    mapping(bytes32 => address) private patientLegalNameAndPatientDIDMap;

    //patient to Hospital Map (patientDID => hospitalDID => hospitalDID)
    mapping(address => mapping(address => address)) patientHospitalMap;

    //Hospital to patient Map (hospitalDID => patientDID => patientDID)
    mapping(address => mapping(address => address)) hospitalPatientMap;


    //Struct for Authorized-Verifiers
    struct AuthorizedVerifier {
        address did;
        string licensedName;
        string licenseNumber;
        string documentHash;
        string verifierKey;
        uint issuedTime;
        uint revokedTime;
        bool isRevoked;
        bool isActive;
        bool isEntity;
    }

    //authorizedVerifierDID => AuthorizedVerifier
    mapping(address => AuthorizedVerifier) private authorizedVerifierMap;

    //patientDID => authorizedVerifierDID => AuthorizedVerifier
    mapping(address => mapping(address => AuthorizedVerifier)) private patientDIDAndVerifierMap;

    event AuthorizationForPatientDID (address indexed did, address indexed verifier);

    event AuthorizationResponse (string authorizationType, address did, string licensedName, string licenseNumber,
                                string documentHash, string verifierKey, uint issuedTime,
                                uint revokedTime, bool isRevoked, bool isActive);

    function getOwner() public view returns(address){
        return owner;
    }

    modifier ownerOnly() {
        require(msg.sender == owner);
        _; 
    }

    function isPatientRegisteredInHospitalNetwork(address _patientDID) public returns(bool isIndeed) {
        return didPatientBlocks[_patientDID].isEntity;
    }

    function doesVerifierExist(address _verifier) public returns(bool isIndeed) {
        return authorizedVerifierMap[_verifier].isEntity;
    }

    function isVerifierAuthorizedForpatientDID(address _patientDID, address _verifier) internal returns(bool isIndeed) {
        return patientDIDAndVerifierMap[_patientDID][_verifier].isEntity;
    }

    function doesHospitalExist(address _hospitalDID) public returns(bool isIndeed) {
        return hospitals[_hospitalDID].isEntity;
    }

    function registerHospital(address _hospitalDID, string memory _name,  string memory _contactNo,
                              string memory _hospitalAddress, string memory _zipcode,
                              string memory _documentHash, string memory _verifierKey)
    public payable ownerOnly  returns(bool)
    {
        require(_hospitalDID!=address(0), "HospitalDID cannot be empty");
        require(bytes(_name).length>0, "HospitalName cannot be empty");
        require(bytes(_contactNo).length>0, "contactNo cannot be empty");
        require(bytes(_hospitalAddress).length>0, "hospitalAddress cannot be empty");
        require(bytes(_zipcode).length>0, "zipcode cannot be empty");
        require(!doesHospitalExist(_hospitalDID), "hospitalDID is already Registered");

        Hospital memory hospitalMemoryItem = Hospital({did: _hospitalDID,
                                name: _name,
                                contactNo: _contactNo,
                                hospitalAddress: _hospitalAddress,
                                zipcode: _zipcode,
                                documentHash: _documentHash,
                                verifierKey: _verifierKey,
                                issuedTime: now,
                                revokedTime: 0,
                                isRevoked: false,
                                isActive: true,
                                isEntity: true});
        hospitals[_hospitalDID] = hospitalMemoryItem;
        bytes32 legalNameInBytes = stringToBytes32(_name);
        hospitalNameAndHospitalDIDMap[legalNameInBytes] = _hospitalDID;
        return true;
    }

    modifier canAccessPatientDIDDetails(address _patientDID, address sender){
        require(( sender == owner ||
                  sender == _patientDID ||
                 (doesVerifierExist(sender) && isVerifierAuthorizedForpatientDID(_patientDID, sender)))
                 , "sender is not authorized to access Patient Details");
        _;
    }

    /*
    This function takes 3 arguments
    in which the DigitalEntity (msg.sender)
    defines the values of Patient parameters. Allowing
    the registration of Patient.
    */
    function addPatientDID(address _patientDID, string memory _name,  string memory _contactNo,
      string memory _documentHash, string memory _verifierKey)
    public payable ownerOnly  returns(bool)
    {
        require(didIssuerInterface.doesDIDExist(_patientDID), "patientDID should have been registered in DIDIssuer Store");
        require(bytes(_name).length>0, "_personalId's cannot be empty");
        require(!isPatientRegisteredInHospitalNetwork(_patientDID), "patientDID is already Registered");
        Patient memory didMemoryItem = Patient({did: _patientDID,
                                name: _name,
                                contactNo: _contactNo,
                                documentHash: _documentHash,
                                verifierKey: _verifierKey,
                                issuedTime: now,
                                revokedTime: 0,
                                isRevoked: false,
                                isActive: true,
                                isEntity: true});
        didPatientBlocks[_patientDID] = didMemoryItem;
        bytes32 legalNameInBytes = stringToBytes32(_name);
        patientLegalNameAndPatientDIDMap[legalNameInBytes] = _patientDID;
        return true;
    }

    function registerPatientDIDForAHospital(address _patientDID, address _hospitalDID)
    public payable ownerOnly  returns(bool)
    {
        require(didIssuerInterface.doesDIDExist(_patientDID), "patientDID should have been registered in DIDIssuer Store");
        require(isPatientRegisteredInHospitalNetwork(_patientDID), "patientDID is not a Registered-Entity in Hospital-Network");
        require(doesHospitalExist(_hospitalDID), "hosiptalDID is not a Registered-Entity in Hospital-Network");
        require(!(patientHospitalMap[_patientDID][_hospitalDID] == _hospitalDID),"Patient is already mapped to the Hospital");
        require(!(hospitalPatientMap[_hospitalDID][_patientDID] == _patientDID),"Hospital is already mapped to the Patient");
        patientHospitalMap[_patientDID][_hospitalDID] = _hospitalDID;
        hospitalPatientMap[_hospitalDID][_patientDID] = _patientDID;
        return true;
    }

    function getPatientDIDDetails(address _patientDidAddress) public view
    //canAccessPatientDIDDetails(_didAddress, address(msg.sender))
                                         returns(address patientDidAddress , string memory name,
                                             string memory contactNo,
                                            string memory documentHash, string memory verifierKey) {
        Patient memory didPatientItem = didPatientBlocks[_patientDidAddress];
        return (didPatientItem.did,didPatientItem.name,
                didPatientItem.contactNo,
                didPatientItem.documentHash, didPatientItem.verifierKey);
    }

    function addVerifier(address _verifier, string memory _licensedName, string memory _licenseNumber,
                         string memory _documentHash, string memory _verifierKey) public ownerOnly returns(bool){
        require(!doesVerifierExist(_verifier), "verifier is an existing Registered entity");
        AuthorizedVerifier memory authorizedVerifierItem = AuthorizedVerifier({did: _verifier,
                                                                                licensedName: _licensedName,
                                                                                licenseNumber: _licenseNumber,
                                                                                documentHash: _documentHash,
                                                                                verifierKey: _verifierKey,
                                                                                issuedTime: now,
                                                                                revokedTime: 0,
                                                                                isRevoked: false,
                                                                                isActive: true,
                                                                                isEntity: true});
        authorizedVerifierMap[_verifier] = authorizedVerifierItem;
        return true;
    }

    function getVerifierDetails(address _verifier) public view returns(address verifierDID,string memory licensedName,
                                                                    string memory licenseNumber,
                                                                    string memory documentHash,
                                                                    string memory verifierKey,
                                                                    uint issuedTime,uint revokedTime,bool isRevoked,
                                                                    bool isActive){
         AuthorizedVerifier memory authorizedVerifierItem = authorizedVerifierMap[_verifier];
        return (authorizedVerifierItem.did,authorizedVerifierItem.licensedName,
                authorizedVerifierItem.licenseNumber, authorizedVerifierItem.documentHash,
                 authorizedVerifierItem.verifierKey,
                authorizedVerifierItem.issuedTime, authorizedVerifierItem.revokedTime,
                authorizedVerifierItem.isRevoked,
                authorizedVerifierItem.isActive);
        }

    function authorizeVerifierForPatientDID(address _patientDID, address _verifier) public returns(address did,string memory licensedName,
                                                                    string memory licenseNumber,
                                                                    string memory documentHash,
                                                                    string memory verifierKey,
                                                                    uint issuedTime,uint revokedTime,bool isRevoked,
                                                                    bool isActive){
        require(didIssuerInterface.doesDIDExist(_patientDID), "patientDID should have been registered in DIDIssuer Store");
        require(isPatientRegisteredInHospitalNetwork(_patientDID),"Patient is not a Registered entity");
        require(doesVerifierExist(_verifier),"verifier is not a Registered entity");
        require(!isVerifierAuthorizedForpatientDID(_patientDID, _verifier),"Verifier is already authorized by Patient");
        address sender = address(msg.sender);
        require((msg.sender == _patientDID || msg.sender == owner),"only owner or patient-did-holder can authorize Verifier");

        patientDIDAndVerifierMap[_patientDID][_verifier] = authorizedVerifierMap[_verifier];
        emit AuthorizationForPatientDID(_patientDID, _verifier);
        AuthorizedVerifier memory authorizedVerifierItem = patientDIDAndVerifierMap[_patientDID][_verifier];
        emit AuthorizationResponse('Patient', authorizedVerifierItem.did,authorizedVerifierItem.licensedName,
                authorizedVerifierItem.licenseNumber, authorizedVerifierItem.documentHash,
                 authorizedVerifierItem.verifierKey,
                authorizedVerifierItem.issuedTime, authorizedVerifierItem.revokedTime,
                authorizedVerifierItem.isRevoked,
                authorizedVerifierItem.isActive);
        return (authorizedVerifierItem.did,authorizedVerifierItem.licensedName,
                authorizedVerifierItem.licenseNumber, authorizedVerifierItem.documentHash,
                 authorizedVerifierItem.verifierKey,
                authorizedVerifierItem.issuedTime, authorizedVerifierItem.revokedTime,
                authorizedVerifierItem.isRevoked,
                authorizedVerifierItem.isActive);
    }

    function getAuthorizedVerifierMappingForDID(address _patientDID, address _verifier) public view returns(address){
       AuthorizedVerifier memory authorizedVerifierItem = patientDIDAndVerifierMap[_patientDID][_verifier];
        return authorizedVerifierItem.did;
    }


    function getDidIssuerAddress() ownerOnly public view returns (address){
        return didIssuerAddress;
    }
    

    function stringToBytes32(string memory source) public pure returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }

        assembly {
            result := mload(add(source, 32))
        }
    }
}