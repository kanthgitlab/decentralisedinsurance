pragma solidity 0.5.16;

interface DIDHospitalInterface {

    function isPatientRegisteredInHospitalNetwork(address _patientDID) external returns(bool isIndeed);

    function doesHospitalExist(address _hospitalDID) external returns(bool isIndeed);
}