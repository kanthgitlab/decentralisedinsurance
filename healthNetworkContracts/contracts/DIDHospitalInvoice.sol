pragma solidity 0.5.16;
import "./DIDIssuerInterface.sol";
import "./DIDHospitalInterface.sol";

//Contract to register/maintain Decentralized-Identities by Hospital-Network
contract DIDHospitalInvoice {
    address payable private owner;
    address private hospitalNetworkDID;
    address private didIssuerAddress;
    address private didHospitalAddress;
    DIDIssuerInterface private didIssuerInterface;
    DIDHospitalInterface private didHospitalInterface;

    //constructor
    constructor() public {
        owner = msg.sender;
    }

     // this function should be called should we ever want to change the didIssuerAddress
    function setDidIssuerAddress(address _didIssuerAddress) ownerOnly public returns (address){
        didIssuerAddress = _didIssuerAddress;
        didIssuerInterface = DIDIssuerInterface(didIssuerAddress);
        return didIssuerAddress;
    }

    function getDidIssuerAddress() public view returns(address){
        return didIssuerAddress;
    }

      // this function should be called should we ever want to change the didHospitalAddress
    function setDidHospitalAddress(address _didHospitalAddress) ownerOnly public returns (address){
        didHospitalAddress = _didHospitalAddress;
        didHospitalInterface = DIDHospitalInterface(_didHospitalAddress);
        return _didHospitalAddress;
    }

    function getDIdHospitalAddress() public view returns (address){
        return didHospitalAddress;
    }

    struct Invoice {
        address patientDID;
        address hospitalDID;
        string invoiceId;
        uint invoiceDate;
        string invoiceCurrency;
        uint256 invoiceAmount;
        string invoiceDocumentHash;
        string verifierKey;
        uint issuedTime;
        uint revokedTime;
        bool isRevoked;
        bool isActive;
        bool isEntity;
    }

    // patientDID => invoiceId => invoiceId
    mapping(address => mapping(string => string)) private patientInvoiceMap;

    //invoiceId => Invoice
    mapping(string => Invoice) private invoices;

    function getOwner() public view returns(address){
        return owner;
    }

    modifier ownerOnly() {
        require(msg.sender == owner);
        _; 
    }

    function issueInvoiceToPatient(address _patientDID, address _hospitalDID, string memory _invoiceId,
                                   string memory _invoiceCurrency, uint256 _invoiceAmount, string memory _invoiceDocumentHash,
                                   string memory _verifierKey ) public payable returns(bool){
        require(didHospitalAdress != address(0), "HospitalInvoice is not initialized with HospitalNetworkContractInterface");
        require(didHospitalInterface.isPatientRegisteredInHospitalNetwork(_patientDID),"Patient is not a Registered entity");
        require(didHospitalInterface.doesHospitalExist(_hospitalDID),"hospital is not a Registered entity");
        require(!invoices[_invoiceId].isEntity, "Invoice is already Issue for Patient");
        Invoice memory invoiceItem = Invoice({patientDID: _patientDID,
                                             hospitalDID: _hospitalDID,
                                             invoiceId: _invoiceId,
                                             invoiceDate: now,
                                             invoiceCurrency: _invoiceCurrency,
                                             invoiceAmount: _invoiceAmount,
                                             invoiceDocumentHash: _invoiceDocumentHash,
                                             verifierKey: _verifierKey,
                                             issuedTime: now,
                                             revokedTime: 0,
                                             isRevoked: false,
                                             isActive: true,
                                             isEntity: true});
        invoices[_invoiceId] = invoiceItem;
        patientInvoiceMap[_patientDID][_invoiceId] = _invoiceId;
        return true;
    }

    function isAnInvoiceIssuedByHospital(address _hospitalDID, string memory _invoiceId) public view returns(bool){
        require(invoices[_invoiceId].isEntity, "Invoice doesn't exist");
        require(didHospitalInterface.doesHospitalExist(_hospitalDID),"hospital is not a Registered entity");
        return invoices[_invoiceId].hospitalDID == _hospitalDID;
    }

    function isAnInvoiceIssuedForPatient(address _patientDID, string memory _invoiceId) public view returns(bool){
        require(invoices[_invoiceId].isEntity, "Invoice doesn't exist");
        require(didHospitalInterface.isPatientRegisteredInHospitalNetwork(_patientDID),"Patient is not a Registered entity");
        string memory invoiceIdFromStorage = patientInvoiceMap[_patientDID][_invoiceId];
        if(keccak256(abi.encodePacked(invoiceIdFromStorage)) == keccak256(abi.encodePacked(_invoiceId))){
            return true;
        }
        return false;
    }

    function getInvoiceDetails(string memory _invoiceId) public view returns(address patientDID,
                                                                      address hospitalDID,
                                                                      string memory invoiceId,
                                                                      string memory invoiceCurrency,
                                                                      uint256 invoiceAmount,
                                                                      string memory invoiceDocumentHash,
                                                                      string memory verifierKey,
                                                                      uint256 issuedTime,
                                                                      bool isRevoked,
                                                                      uint256 revokedTime){
        require(invoices[_invoiceId].isEntity, "Invoice doesn't exist");
        Invoice memory invoiceItem = invoices[_invoiceId];
        return (invoiceItem.patientDID,
                invoiceItem.hospitalDID,
                invoiceItem.invoiceId,
                invoiceItem.invoiceCurrency,
                invoiceItem.invoiceAmount,
                invoiceItem.invoiceDocumentHash,
                invoiceItem.verifierKey,
                invoiceItem.issuedTime,
                invoiceItem.isRevoked,
                invoiceItem.revokedTime);
    }

    function stringToBytes32(string memory source) public pure returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }

        assembly {
            result := mload(add(source, 32))
        }
    }
}