const router = require('express').Router();
const digitalEntityController = require('../controllers/DigitalEntityController');

/* Events api path */
router.route('/did').post(digitalEntityController.addDigitalEntity);
router.route('/did').get(digitalEntityController.getAllDigitalEntity);

module.exports = router;
