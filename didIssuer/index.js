const express = require('express');
//const mongoose = require('mongoose');
const cors = require('cors');
const eventRoutes = require('./src/routes/DigitalEntityRoutes');
const db = require('./src/config/db');

const app = express();
const PORT = process.env.PORT || 5000;

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));

// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/*+json' }))

// parse some custom thing into a Buffer
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }))

// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }))

app.use(express.json());

app.use("/api/v1", eventRoutes)
    .use(function (req, res) {
        return res.status(404).send({message: 'Route' + req.url + ' Not found.'});
    })
    .use(cors);

app.listen(PORT, () => console.log('Application listening in port ', PORT));

module.exports = app;
