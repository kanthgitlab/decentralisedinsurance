const ipfsClient = require('ipfs-http-client');
const logger = require('../common/logger')
const ipfsAPI = require('ipfs-api');
const IpfsFileModel = require('../models/IPFSFileModel');

//Connceting to the ipfs network via infura gateway
const ipfs = ipfsAPI({host: 'localhost', port: '5001', protocol: 'http'});

const addFileToIPFS = async ({content }) => {
    logger.info(`content: ${content}`);
    let buffer = Buffer.from(content,'utf-8');
    logger.info(`buffer: ${buffer}`);
    let fileAddResponse = await ipfs.files.add(buffer);
    logger.info(`ipfsResponse: ${fileAddResponse}`);
    let ipfsFileModel = new IpfsFileModel();
    ipfsFileModel.ipfsHash = fileAddResponse[0].hash;
    ipfsFileModel.path = `https://gateway.ipfs.io/ipfs/${fileAddResponse[0].hash}`;
    return ipfsFileModel;
}

const getFileFromIPFS = async ({ hash }) => {
    const fileContent = await ipfsClient.from(hash);
    let ipfsFileModel = new ipfsFileModel()
    ipfsFileModel.fileContent = fileContent[0].fileContent;
    ipfsFileModel.ipfsHash = fileContent[0].hash;
    return ipfsFileModel;
}

module.exports = {addFileToIPFS, getFileFromIPFS}