pragma solidity 0.5.16;
pragma experimental ABIEncoderV2;

//Contract to register/maintain Decentralized-Identities by Government

contract DIDInsurancePolicy {
    address payable private owner;

    mapping(bytes32 => address) private legalNameDIDMap;

    mapping(address => DID) private didBlocks;

    mapping(address => AuthorizedVerifier) private authorizedVerifierMap;

    mapping(address => mapping(address => AuthorizedVerifier)) private didAndVerifierMap;

    event AuthorizationForDID (address indexed did, address indexed verifier);

    event AuthorizationResponse (address did, string licensedName, string licenseNumber,
                                string documentHash, string verifierKey, uint issuedTime,
                                uint revokedTime, bool isRevoked, bool isActive);

    //Struct for DIDBlock
    struct DID {
        address did;
        string name;
        string personalId;
        string contactNo;
        string category;
        string documentHash;
        string verifierKey;
        uint issuedTime;
        uint revokedTime;
        bool isRevoked;
        bool isActive;
        bool isEntity;
    }

    //Struct for Authorized-Verifiers
    struct AuthorizedVerifier {
        address did;
        string licensedName;
        string licenseNumber;
        string documentHash;
        string verifierKey;
        uint issuedTime;
        uint revokedTime;
        bool isRevoked;
        bool isActive;
        bool isEntity;
    }

    //constructor
    constructor() public {
        owner = msg.sender;
    }

    function getOwner() public view returns(address){
        return owner;
    }

    function doesDIDExist(address _did) internal returns(bool isIndeed) {
        return didBlocks[_did].isEntity;
    }

    function doesVerifierExist(address _verifier) internal returns(bool isIndeed) {
        return authorizedVerifierMap[_verifier].isEntity;
    }

    function isVerifierAuthorizedForDID(address _did, address _verifier) internal returns(bool isIndeed) {
        return didAndVerifierMap[_did][_verifier].isEntity;
    }


    modifier ownerOnly() {
        require(msg.sender == owner);
        _; 
    }

    /*
    This function takes 3 arguments
    in which the DigitalEntity (msg.sender)
    defines the values of DID parameters. Allowing
    the registration of DIDEntry.
    */
    function registerDID(address _did, string memory _name, string memory _personalId,  string memory _contactNo,
      string memory _category, string memory _documentHash, string memory _verifierKey)
    public payable ownerOnly  returns(bool)
    {   
        require(bytes(_personalId).length>0, "_personalId's cannot be empty");
        require(!doesDIDExist(_did), "DID is already Registered");
        bytes32 legalNameInBytes = stringToBytes32(_name);
        DID memory didMemoryItem = DID({did: _did,
                                name: _name,
                                personalId: _personalId,
                                contactNo: _contactNo,
                                category: _category,
                                documentHash: _documentHash,
                                verifierKey: _verifierKey,
                                issuedTime: now,
                                revokedTime: 0,
                                isRevoked: false,
                                isActive: true,
                                isEntity: true});
        didBlocks[_did] = didMemoryItem;
        legalNameDIDMap[legalNameInBytes] = _did;
        return true;
    }

    modifier canAccessDIDDetails(address _didAddress, address sender){
        require(( sender == owner ||
                  sender == _didAddress ||
                 (doesVerifierExist(sender) && isVerifierAuthorizedForDID(_didAddress, sender)))
                 , "sender is not authorized to access DID Details");
        _;
    }

    function getDIDDetails(address _didAddress) public view
    //canAccessDIDDetails(_didAddress, address(msg.sender))
                                         returns(address didAddress , string memory name,
                                            string memory personalId, string memory contactNo,
                                             string memory category,
                                            string memory documentHash, string memory verifierKey) {
        DID memory didItem = didBlocks[_didAddress];
        return (didItem.did,didItem.name,
                didItem.personalId, didItem.contactNo, didItem.category,
                didItem.documentHash, didItem.verifierKey);
    }

    function addVerifier(address _verifier, string memory _licensedName, string memory _licenseNumber,
                         string memory _documentHash, string memory _verifierKey) public ownerOnly returns(bool){
        require(!doesVerifierExist(_verifier), "verifier is an existing Registered entity");
        AuthorizedVerifier memory authorizedVerifierItem =
                        AuthorizedVerifier({did: _verifier,
                                licensedName: _licensedName,
                                licenseNumber: _licenseNumber,
                                documentHash: _documentHash,
                                verifierKey: _verifierKey,
                                issuedTime: now,
                                revokedTime: 0,
                                isRevoked: false,
                                isActive: true,
                                isEntity: true});
        authorizedVerifierMap[_verifier] = authorizedVerifierItem;
        return true;
    }

    function getVerifierDetails(address _verifier) public view returns(address did,string memory licensedName,
                                                                    string memory licenseNumber,
                                                                    string memory documentHash,
                                                                    string memory verifierKey,
                                                                    uint issuedTime,uint revokedTime,bool isRevoked,
                                                                    bool isActive){
         AuthorizedVerifier memory authorizedVerifierItem = authorizedVerifierMap[_verifier];
        return (authorizedVerifierItem.did,authorizedVerifierItem.licensedName,
                authorizedVerifierItem.licenseNumber, authorizedVerifierItem.documentHash,
                 authorizedVerifierItem.verifierKey,
                authorizedVerifierItem.issuedTime, authorizedVerifierItem.revokedTime,
                authorizedVerifierItem.isRevoked,
                authorizedVerifierItem.isActive);
        }

    function authorizeVerifier(address _did, address _verifier) public returns(address did,string memory licensedName,
                                                                    string memory licenseNumber,
                                                                    string memory documentHash,
                                                                    string memory verifierKey,
                                                                    uint issuedTime,uint revokedTime,bool isRevoked,
                                                                    bool isActive){
        address sender = address(msg.sender);
        require(doesDIDExist(sender),"DID is not a Registered entity");
        require(doesVerifierExist(_verifier),"verifier is not a Registered entity");
        require(!isVerifierAuthorizedForDID(_did, _verifier),"Verifier is already authorized by DID");
        require((msg.sender == _did || msg.sender == owner),"only owner or did-holder can authorize Verifier");

        didAndVerifierMap[_did][_verifier] = authorizedVerifierMap[_verifier];
        emit AuthorizationForDID(_did, _verifier);
        AuthorizedVerifier memory authorizedVerifierItem = didAndVerifierMap[_did][_verifier];
        emit AuthorizationResponse(authorizedVerifierItem.did,authorizedVerifierItem.licensedName,
                authorizedVerifierItem.licenseNumber, authorizedVerifierItem.documentHash,
                 authorizedVerifierItem.verifierKey,
                authorizedVerifierItem.issuedTime, authorizedVerifierItem.revokedTime,
                authorizedVerifierItem.isRevoked,
                authorizedVerifierItem.isActive);
        return (authorizedVerifierItem.did,authorizedVerifierItem.licensedName,
                authorizedVerifierItem.licenseNumber, authorizedVerifierItem.documentHash,
                 authorizedVerifierItem.verifierKey,
                authorizedVerifierItem.issuedTime, authorizedVerifierItem.revokedTime,
                authorizedVerifierItem.isRevoked,
                authorizedVerifierItem.isActive);
    }

    function getAuthorizedVerifierMappingForDID(address _did, address _verifier) public view returns(address){
       AuthorizedVerifier memory authorizedVerifierItem = didAndVerifierMap[_did][_verifier];
        return authorizedVerifierItem.did;
    }

    function stringToBytes32(string memory source) public pure returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }

        assembly {
            result := mload(add(source, 32))
        }
    }
}