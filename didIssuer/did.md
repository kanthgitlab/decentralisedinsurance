# Reference for DID:

 - Decentralized / Distributed Identity is a new Decentralized 


## Decentralized Identity:

Decentralized Identity is a global unique Identity which is 
- portal
- unique
- immutable
- non-centralized
- global

## Siloed Central Identity:
 - centralized Identity provided by the app or web-application


## 3rd Party Identity:
 - 3rd Party Provider to manage credentials
 - login-Provider manage all login to consented platforms
 - examples: social Login on facebook, google
 - You don't exist on internet with-out the 3rd Party Identity

## Distributed Identity(DPKI):

 - Peer-Peer (P2P) Identity Verification
 - You are the first class citizen on Internet
 - You can identify as well establish relationships and associations

### Open Standards (SSI):

 1. DID (Decentralized Identifier) [W3C]
 2. DKMS (Decentralized Key Management System) [OASIS]
 3. DIDAUTH (Decentralized Authentication) [DIF - IETF]
 4. Verifiable Credentials  [W3C]

### DID - Sovrin:

 - 