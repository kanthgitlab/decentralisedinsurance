/* eslint-disable */
const moment = require("moment");

const pino = require('pino')({
  mixin() {
    return { timestamp: moment().toJSON() }
  },
  prettyPrint: true,
  base: null,
  useLevelLabels: true
});

let logger = pino;

 const log = (msg, ...args) => {
  logger.info(msg, ...args);
};

 const error = (msg, ...args) => {
  logger.error(msg, ...args);
};

 const warn = (msg, ...args) => {
  logger.warn(msg, ...args);
};

 const debug = (msg, ...args) => {
  logger.debug(msg, ...args);
};

 const info = (msg, ...args) => {
  logger.info(msg, ...args);
};

const handleArgs = (msg, args) => {
  if (args) {
    for (const arg of args) {
      let data = arg;
      if (data instanceof Error) {
        data = arg.stack;
      }
      if (msg.indexOf('%s') < 0) {
        msg = msg + '\n' + data;
      } else {
        msg = msg.replace('%s', data);
      }
    }
  }
  return msg;
};

module.exports ={pino, log, error, warn, debug, info, handleArgs}