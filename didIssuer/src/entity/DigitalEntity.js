const mongoose = require('mongoose');

const {Schema} = mongoose;

const DigitalEntitySchema = new Schema(
    {
        did: {type: String, required: true},
        name: {type: String, required: false},
        personalId: {type: String, required: false},
        contactNo: {type: Number, required: false},
        verifyKey: {type: String, required: false},
        encryptionPublicKey: {type: String, required: false},
        documentHash: {type: String, required: false},
        documentPath: {type: String, required: false},
        documentMetaHash: {type: String, required: false},
        documentType: {type: String, required: false},
        isRevoked: {type: Boolean, required: true, default: false},
        revokedDate: { type: Date, required: false}
    },
    {
        timestamps: true,
    },
);

const DigitalEntity = mongoose.model('DigitalEntity', DigitalEntitySchema);

module.exports = DigitalEntity;