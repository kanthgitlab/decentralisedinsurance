const digitalEntityService = require('../services/DigitalEntityService');
const responseDataUtil = require('../util/ResponseDataUtil');
const logger = require("../common/logger");

const digitalEntityController = {

    /**
     * Add a digitalEntity in system.
     * @param request
     * @param response
     * @returns {Promise<void>}
     */
    addDigitalEntity: async (request, response) => {
        logger.info(`request.body: ${request.body.personalId}`);
        let responseData = await digitalEntityService.createDigitalEntity(request.body);
        responseDataUtil.updateResponse(response, responseData);
    },

    /**
     * Fetch all digitalEntitys in system.
     * @param request
     * @param response
     * @returns {Promise<void>}
     */
    getAllDigitalEntity: async (request, response) => {
        let responseData = null;
        if (request.query.personalId) {
            responseData = await digitalEntityService.findByParams(request.query.personalId);
        } else {
            responseData = await digitalEntityService.findAllDigitalEntities();
        }
        responseDataUtil.updateResponse(response, responseData);
    },

    /**
     * Find a DigitalEntity by digitalEntityId or _id.
     * @param request
     * @param response
     * @returns {Promise<void>}
     */
    getOneById: async (request, response) => {
        let responseData = await digitalEntityService.findOneById(request.params.id);
        responseDataUtil.updateResponse(response, responseData);
    },


    /* Only for testing */
    testDigitalEntity: (req, res) => {
        digitalEntityService.test();
        res.status(200).json({
            success: true,
            message: 'This is test message.',
        });
    }

};

module.exports = digitalEntityController;
