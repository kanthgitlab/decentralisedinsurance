var didHospital = artifacts.require("./DIDHospital.sol");
var didHospitalInvoice = artifacts.require("./DIDHospitalInvoice.sol");

module.exports = function(deployer) {
  deployer.deploy(didHospital);
  deployer.deploy(didHospitalInvoice);
};